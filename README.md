This project folder is exclusively for managing the templates used to create the following issues:

- [Onboarding](.gitlab/issue_templates/onboarding.md)
- [Offboarding](.gitlab/issue_templates/offboarding.md)
- [Career Mobility](.gitlab/issue_templates/career_mobility.md)
- [People Experience Team Shadow](.gitlab/issue_templates/people_experience_team_shadow.md)
- [Relocation](.gitlab/issue_templates/relocation.md)

Issues related to temporary service providers (short term contractors and consultants) should be open in the [Temporary Service Providers group](https://gitlab.com/gitlab-com/temporary-service-providers/lifecycle). 

If you would like to contribute to these templates, please create a Merge Request within this Project and a member of the People Experience Team will review and approve ahead of merging.

All issues relating to Onboarding; Offboarding; Career Mobility (Cross-Boarding) and Relocations are housed in the [Team Member Epics Project](https://gitlab.com/gitlab-com/team-member-epics/) once they have been created.

## Tips for creating an Onboarding, Offboarding or Career Mobility Issue:

- Tasks which are specific to a **Country; Division; Department or Entity** should not be inserted into the **Master or General Onboarding Template** but rather to the unique template created for that particular purpose (examples below):
    - If an Onboarding Task relevant to People Success team members needs to be inserted or updated this would be carried our in the 'department_people_success.md' file - should the desired file not exist one can be created within the 'onboarding_tasks' directory.
    - If an Offboarding task relevant to Engineering Division team members needs to inserted or updated this would be carried out in the 'division_engineering.md' file - should the desired file not exist one can be added to the 'offboarding_tasks' directoy.

Should a new file need to be created, it should always be created with the following format in mind 'key_value.md' with key being either Country; Entity; Department or Role and Value being the specifics around that key for example 'country_netherlands'.  It is important to note that the value inserted must be exactly as it is detailed on BambooHR to ensure existing or future automations will work.

If you feel uncertain in terms of creating an issue or making an update please feel free to reach out to the People Experience Team via the #people-exp-ops channel on Slack and remember everyone can contribute!
