### Welcome to your Career Mobility Issue!

## Introduction

The Career Mobility Issue which was compiled to offer you support and guidance as you cross-board into your new role within GitLab, is split out into four areas of responsibility - namely those of the **Team Member** (__TEAM_MEMBER_HANDLE__); those of the **Previous Reporting Manager** (__PREVIOUS_MANAGER_HANDLE__); those of the **New Reporting Manager** (__MANAGER_HANDLE__) and finally those of the **People Experience Associate Team** (__PEOPLE_EXPERIENCE_HANDLE__).

If any of the tasks assigned to you seem confusing and you aren't sure how to proceed please be sure to let the People Experience Team know via the #peopleops Slack channel.  Alternatively if you are unable to work on a task because you are waiting on another stakeholder to close out one of theirs, feel free to ping them right here in this issue.  

In some instances your **New Reporting Manager** may opt to assign you a **Career Mobility Buddy** as an extra point of support, if this is the case please don't hesitate to route any queries you may have in their direction too.

### What to Expect

The People Experience Team has identified a set of tasks that will make your cross-boarding easy and efficient.  You are encouraged to complete each task over the course of two weeks paying careful attention to those which are compliance related (more information on that below).

| Area of Focus |
| --- |
| Profile Updates (Internal and External) |
| GitLab Values Check-In (Career Mobility) |
| Career Mobility Handover |
| Competency Refreshers |
| Role Based Tasks |

### Cross-Boarding Compliance
It is important to note that certain tasks within your Career Mobility Issue relate to compliance and ultimately keeping GitLab secure.  These tasks are marked with a **red circle** and completion of them is subject to audit by both the People Experience Team and various other stakeholders such as **Security** and **Payroll** so please be sure to acknowledge them once complete by checking the relevant box.

### Continuous Improvement
In the interests of paying it forward and contributing to our core values of Collaboration and Efficiency - we encourage you to suggest changes and updates to this issue and the handbook all of which can be done via Merge Request (MR).

-----

### Pre Cross-Boarding Tasks

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Once the People Experience Associates have been notified by the People Operations Analyst in the Promotion/Transfer Tracker spreadsheet that migration of a team member into a new role is forthcoming, create a **confidential** issue called 'Career Mobility (NAME), per (DATE, please follow yyyy-mm-dd) as (TITLE)' in the [People Ops Employment Issue Tracker](https://gitlab.com/gitlab-com/people-ops/employment/issues) with relevant sections of this checklist, be it Promotion, Demotion or Migration to a New Role.
1. [ ] People Experience: Check if the due date is correctly added (two weeks from the effective date of migration i.e. the date on which the team member will start their new role) by the Employment bot.
1. [ ] People Experience: Previous Manager is `__PREVIOUS_MANAGER_HANDLE__`, new Manager is `__MANAGER_HANDLE__`, and People Experience is tackled by `__PEOPLE_EXPERIENCE_HANDLE__`. Check if the issue is assigned to the People Experience team member, the migrating team member, both the Previous and New Reporting Managers and the relevant People Business Partner.
1. [ ] People Experience: Check that the New Manager has linked the relevant access requests in this issue for compliance.

</details>

<details>
<summary>Previous Reporting Manager</summary>

1. [ ] Previous Reporting Manager: Review the team member's current access and submit an [Access Change Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Access_Change_Request) issue. Please work with the New Reporting Manager to determine if a team member's access will still be needed in the new role or if access will need to be updated to a higher/lower access role. Please cc the new manager.
1. [ ] Coordinate with IT Ops to change any shared passwords, with the intent to be gated behind Okta when possible, in particular;
   1. [ ] Review if team member has sysadmin access passwords for GitLab.com Infrastructure (ssh, chef user/key, others). Identify if any can be moved to Okta.
   1. [ ] Review what 1Password vaults team member had access to, and identify any shared passwords to be changed and moved to Okta. If team member has access to any shared passwords that will no longer be relevant to their new role, please ping  `@gitlab-com/business-ops/team-member-enablement` and coordinate shared account password rotation.
1. [ ] Previous Reporting Manager: If they will become or will no longer be a provisioner of a tool, open an issue to [Update the Tech Stack Provisioner.](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Update_Tech_Stack_Provisioner) Please cc the new manager.
1. [ ] Previous Reporting Manager: If a recent 360 feedback review has been completed, arrange a handover with the New Reporting Manager to provide the Team Members feedback received and identified growth areas.  

</details>

<details>
<summary>New Reporting Manager</summary>

1. [ ] Manager: Review the team member's current access and submit an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Single_Person_Access_Request) or choose from a role based template in the [new issue dropdown](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues).
1. [ ] Manager: Be sure to link any Access Requests created to this Career Mobility Issue for ease of reference and in support of internal security compliance mechanisms.
1. [ ] Manager: Select an Career Mobility Buddy on the new employee's team.  Please try to select someone in a similar time zone, and someone who has been at GitLab for at least 3 months.
1. [ ] Manager: Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been chosen, and include a link to the onboarding issue.
1. [ ] Manager: Schedule a video call using Zoom with the new team member at the start of their first day to welcome them to the team and set expectations.
1. [ ] Manager: Organize smooth onboarding with clear starting tasks / pathway for new team member.
1. [ ] Calendars & Agenda
   1. [ ] Manager: Invite team member to recurring team meetings.
   1. [ ] Manager: Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with new team member. Use the first 15 minutes to get to know your new team member on a personal level.
1. [ ] Manager: If a recent 360 feedback review has been completed, arrange a handover from the Previous Manager to provide the Team Members feedback received and identified growth areas. 
1. [ ] Manager: Set new GitLab team members' project-level permissions as-needed.
1. [ ] Manager: Add the new team member to the required groups in 1Password if you have access to the Admin Console in 1Password. If you do not have access please ping @gitlab-com/business-ops/team-member-enablement with which vaults the new team member should be added to. Note: if it is necessary to add an individual to a vault (instead of to a group), make sure that the permissions are set to _not allow_ exporting items.
1. [ ] Manager: Introduce your new team member to your team at your next team call. Ask them to tell the team about where they were before GitLab, why they wanted to join the team, where they are located, and what they like to do for fun outside of work.

</details>

<details>
<summary>Career Mobility Buddy</summary>

1. [ ] Buddy: Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) to get familiar with your role. These guidelines were written for onboarding but many can still apply to assist in career mobility. 
1. [ ] Buddy: Schedule a zoom call for their first day to introduce yourself. Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc.
1. [ ] Buddy: Schedule at least one follow-up chat after one week. 

</details>

### Team Member Tasks

<details>
<summary>Team Member</summary>

#### Career Mobility Values Check-In

1. [ ] Team Member: Complete the [Values Check-In (Career Mobility)](https://docs.google.com/forms/d/e/1FAIpQLSciWfxj_Wj0IgbVzylpPKM9WEFyc0z4sD0cN6GfAn9tNyQi_A/viewform?usp=sf_link) which centers predominantly on the role you will be transitioning from.

#### Competency Refresher

Review the [Competencies Handbook Page](https://about.gitlab.com/handbook/competencies/) which highlights three key categories of competency within GitLab namely [Values Competencies](https://about.gitlab.com/handbook/competencies/#values-competencies); [Remote Work Competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies) and [Functional Competencies](https://about.gitlab.com/handbook/competencies/#functional-competencies).

1. [ ] If you have not done so already, complete the [GitLab All-Remote Certification](https://about.gitlab.com/company/culture/all-remote/remote-certification/) (Remote Work Foundation) which was designed to give New Managers and Individual Contributors an opportunity to master all-remote business concepts and build key skills in remote subject areas.
1. [ ] If you have not done so already, complete the [Diversity; Inclusion and Belonging Learning Path](https://gitlab.com/gitlab-com/diversity-and-inclusion/-/blob/master/.gitlab/issue_templates/diversity-inclusion-belonging-training-template.md) which currently includes the LinkedIn Learning Diversity, Inclusion, and Belonging for All Training.
1. [ ] If you have not done so already, complete the [GitLab 101 Tool Certification](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/gitlab-101/#introduction).

#### Profile Updates 

1. [ ] Team Member: Update team page entry. Instructions here in [the handbook](https://about.gitlab.com/handbook/git-page-update/#11-add-yourself-to-the-team-page).
1. [ ] Team Member: Update your [Slack profile](https://gitlab.slack.com/account/profile) to include your new role.
1. [ ] Team Member: Update your GitLab profile to include your new role.
1. [ ] Team Member: Update your Gmail signature to include your new role. [Here is an example you can use](https://about.gitlab.com/handbook/tools-and-tips/#sts=Email%20signature).
1. [ ] Team Member: Update your [Zoom profile](https://zoom.us/profile) to include your new role.

</details>

---

### Role-specific tasks

#### People Managers

<details>
<summary>Team Member</summary>

1. [ ] Team Member: Review [Git page update](https://about.gitlab.com/handbook/git-page-update/) to learn more about contributing and using GitLab.
1. [ ] Team Member: Make a meaningful [update to the handbook](https://about.gitlab.com/handbook/handbook-usage/) and assign the merge request to your manager.
1. [ ] Team Member: If applicable, review the [Vacancy Creation Process](https://about.gitlab.com/handbook/hiring/vacancies/), to learn how to open a new position.
1. [ ] Team Member: Review the [Leadership handbook page](https://about.gitlab.com/handbook/leadership/), particularly the recommended [articles](https://about.gitlab.com/handbook/leadership/#articles) and [books](https://about.gitlab.com/handbook/leadership/#books).

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Give member `Maintainer` access on [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com).
1. [ ] Manager: Check if a new [becoming a gitlab manager](https://gitlab.com/gitlab-com/people-group/Training/-/issues/new?issuable_template=becoming-a-gitlab-manager) issue exists and is assigned to the team member.  This lives in the [training issues](https://gitlab.com/gitlab-com/people-ops/Training/issues) project and should be created automatically with the career mobility issue.  Provide a link to the issue in a comment below this onboarding checklist.
1. [ ] Manager:  Before the team member's effective date, update the reporting structure in BambooHR by completing the following steps:
   * Login into BambooHR and click the employees tab
   * Select the applicable employee(s)
   * If you cannot see the employees in that view, search for them in the Search bar
   * Once in the employee profile, on the upper right hand side, click the request a change drop down.
   * Select job information, and complete the updated fields, including the 'reports to' field.
   * People Ops will process the request and it will be updated shortly.
1. [ ] Manager: Before the effective date, update the reporting structure on the team page.
1. [ ] Manager: Comment in the issue if the team member needs an Interview Training Issue and/or additional permissions in Greenhouse.
1. [ ] Manager: Submit an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=slack_googlegroup_1Passwordgroupvault) to have the team member added to the Manager Google Group.

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience : If applicable, create an [interviewing training issue](https://gitlab.com/gitlab-com/people-ops/Training/blob/master/.gitlab/issue_templates/interview_training.md) in the [People Ops Training project](https://gitlab.com/gitlab-com/people-ops/Training/issues) and assign it to the team member.
1. [ ] Recruitings Operations & Insights (@gl-recruitingops) : If applicable, [upgrade team member in Greenhouse to either "Interviewer" or "Job Admin: Hiring Manager"](https://about.gitlab.com/handbook/hiring/greenhouse/#access-levels-and-permissions) for any roles they will be interviewing for or a hiring manager for. If they will be opening new vacancies in the future, they will need to have the permission "Can create new jobs and request approvals" enabled. If they are an executive, ping Recruiting in order to add them to the Greenhouse vacancy and offer approval flow for their division.

</details>

---

<!-- include: division_tasks -->

---

<!-- include: department_tasks -->

---

<!-- include: role_tasks -->


/label ~"career-mobility"
