## Sales Development Representatives Section

<details>
<summary>New Reporting Manager</summary>

1. [ ] Manager: List name and email:
2. [ ] Manager: List SDR name and email: 
3. [ ] Manager: Select the segment the new team member will belong to:
    * [ ] SMB
    * [ ] Mid-Market
    * [ ] Large 
    * [ ] Public Sector
    * [ ] Named
4. [ ] Manager: Select the region the new team member will belong to:
    * [ ] APAC
    * [ ] AMER
    * [ ] EMEA
    * [ ] LATAM
5. [ ] Manager: Fill out and submit [this issue template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=leandata_change_sdralignment) to request Marketing Operations update LeanData routing, the SSoT alignment spreadsheet, Drift routing/team, and the `SDR Assigned` Salesforce field. Confirmation of completion will be stated on the issue prior to close.
6. [ ] Manager: Fill out and submit [this issue template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single_Person_Access_Request) to remove new SDR from old Slack user group and add them to your slack user group (at mention group for your team). [Here](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/5446) as an example of how to fill it out (replace google group in example with removing SDR from old Slack user group).
7. [ ] Manager: Update the `reports to` line to have your `slug` listed next to the SDR's name in the [Org chart](https://gitlab.com/gitlab-com/www-gitlab-com/d23e8dfb254cca97b87e82554cb4f7d817720c85/handbook/marketing/website#updating-the-team-page-and-org-chart).
8. [ ] Manager: Add any additional info if needed:

</details>

<details>
<summary>Sales Systems</summary>

1. [ ] Sales Systems: Update SFDC Manager 
1. [ ] Sales Systems: Update SFDC role
1. [ ] Sales Systems: Update Chorus role
1. [ ] Sales Systems: Validation rules and sharing settings

</details>

/label ~SalesSystems
/assign @Astahn
