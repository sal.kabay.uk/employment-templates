#### People Managers

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Review [Git page update](https://about.gitlab.com/handbook/git-page-update/) to learn more about contributing and using GitLab.
1. [ ] New team member: Make a meaningful [update to the handbook](https://about.gitlab.com/handbook/handbook-usage/) and assign the merge request to your manager.
1. [ ] New team member: If applicable, review the [Vacancy Creation Process](https://about.gitlab.com/handbook/hiring/recruiting-framework/req-creation/), to learn how to open a new position.
1. [ ] New team member: Review the [Leadership handbook page](https://about.gitlab.com/handbook/leadership/), particularly the recommended [articles](https://about.gitlab.com/handbook/leadership/#articles) and [books](https://about.gitlab.com/handbook/leadership/#books).
1. [ ] New team member: Be sure to review our [gift guidelines](https://about.gitlab.com/handbook/people-group/#gifts) so you request flowers and other items for your team members for appropriate occasions.
1. [ ] New team member: Be sure to complete New Manager training. You can find the link to your issue in the comments.

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: Give member `Maintainer` access on [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com).
1. [ ] Manager:  Before the new hire's first day, update the reporting structure in BambooHR by completing the following steps:
   * Login into BambooHR and click the employees tab
   * Select the applicable employee(s)
   * If you cannot see the employees in that view, search for them in the Search bar
   * Once in the employee profile, on the upper righthand side, click the request a change drop down.
   * Select job information, and complete the updated fields, including the 'reports to' field.
   * People Experience will process the request and it will be updated shortly.
1. [ ] Manager: Comment in the issue if the new team member needs an Interview Training Issue and/or additional permissions in Greenhouse.
1. [ ] Manager: Submit an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Single_Person_Access_Request) to have the team member added to the applicable manager groups below:
   * [Managers Google Group](https://groups.google.com/a/gitlab.com/g/managers/members)
   * Backend Managers: [`@gitlab-com/backend-managers`](https://gitlab.com/groups/gitlab-com/backend-managers/-/group_members?with_inherited_permissions=exclude) GitLab group
   * Frontend Managers: [`@gitlab-org/frontend/frontend-managers`](https://gitlab.com/groups/gitlab-com/frontend/frontend-managers/-/group_members?with_inherited_permissions=exclude) GitLab group
   * Infrastructure Managers: [`@gitlab-com/gl-infra/managers`](https://gitlab.com/groups/gitlab-com/gl-infra/managers/-/group_members?with_inherited_permissions=exclude) GitLab group
   * UX Managers: [`@gitlab-com/gitlab-ux/managers`](https://gitlab.com/groups/gitlab-com/gitlab-ux/managers/-/group_members?with_inherited_permissions=exclude)
   * Security Managers: [`@gitlab-com/gl-security/security-managers`](https://gitlab.com/groups/gitlab-com/gl-security/security-managers/-/group_members?with_inherited_permissions=exclude)
   * Support Managers: [`@gitlab-com/support/managers`](https://gitlab.com/groups/gitlab-com/support/managers/-/group_members?with_inherited_permissions=exclude) GitLab group

*Note: Not all people managers should be added to the groups above. This is managed on a role-specific and department/division specific basis. If you are unclear regarding which group(s) to add your team member to, please check with your manager.*

</details>


<details>
<summary>People Experience</summary>

1. [ ] People Experience: If applicable, create an [interviewing training issue](https://gitlab.com/gitlab-com/people-group/Training/blob/master/.gitlab/issue_templates/interview_training.md) in the [People Operations Training project](https://gitlab.com/gitlab-com/people-group/Training/issues) and assign it to the new team member. Provide a link to the issue in a comment below this onboarding checklist.
1. [ ] People Experience: If applicable, create an [becoming a GitLab manager issue](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md) in the [People Operations Training project](https://gitlab.com/gitlab-com/people-group/Training/issues) and assign it to the new team member. Provide a link to the issue in a comment below this onboarding checklist.

</details>


<details>
<summary>Recruiting Operations</summary>

1. [ ] Recruiting Operations (@gl-recruitingops): Add team member to Greenhouse with `Basic` permissions. If the team member will be on a Hiring Team (e.g. an *Interviewer* or *Hiring Manager*), [upgrade their permissions](https://about.gitlab.com/handbook/hiring/greenhouse/#access-levels-and-permissions) to either `Interviewer` or `Job Admin: Hiring Manager` for the applicable Division or Department.

</details>
