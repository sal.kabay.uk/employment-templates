#### Technical Account Managers

<details>
<summary>New Team Member</summary>

1. [ ] Request a [Light Agent account in Zendesk](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff).
1. [ ] Shadow/pair with a [Support team member](https://about.gitlab.com/team/) (appropriate to timezone) on 3-5 calls.
1. [ ] Shadow fellow Technical Account Managers on 4 customer calls.
1. [ ] Learn about environment Information and [maintenance checks](http://docs.gitlab.com/ce/raketasks/maintenance.html).
1. [ ] Learn about the [GitLab check](http://docs.gitlab.com/ce/raketasks/check.html) rake command.
1. [ ] Learn about GitLab Omnibus commands (`gitlab-ctl`).
   1. [ ] [GitLab Status](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#get-service-status).
   1. [ ] [Starting and stopping GitLab services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-and-stopping).

</details>
