#### Engineering Division

<details>
<summary>New Team Member</summary>

1. [ ] New team member: (For Engineering) Read the [Developer Onboarding](https://about.gitlab.com/handbook/developer-onboarding/)
1. [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] New team member: Purchase and expense for yourself a [YubiKey 5 Series](https://www.yubico.com/products/yubikey-5-overview/) or greater.
1. [ ] New Team Member: Set a reminder to add yourself as a code reviewer for GitLab after working here for 3 months. You can do this by adding the appropriate entries to the `data/team.yml` file with which you added yourself to the team page. Here's an [example for frontend](https://gitlab.com/gitlab-com/www-gitlab-com/blob/f682eaf/data/team.yml#L1378-1379), and an [example for backend](https://gitlab.com/gitlab-com/www-gitlab-com/blob/07df3da/data/team.yml#L10685-10686). Doing reviews is a good way to help your teammates, improve GitLab, and learn more about the code base. Because we use [reviewer roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette) once you add yourself as a reviewer, people will assign merge requests for you to review.
1. [ ] New Team Member: Ensure that you've [requested to be added to  chatops for Gitlab.com administrative tasks](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#requesting-access).
1. [ ] New Team Member: Try connecting to the [staging rails console via ssh](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/bastions/gstg-bastions.md). This can be helpful for verifying fixes during deployment.
1. [ ] New Team Member: Review the [tech stack spreadsheet](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0) to see what systems are available to you, or other team members that can help you use them.
1. [ ] New Team Member: Check to see that you have been added to Engineering Google Group `engineering@gitlab.com`. Google Groups and members are reported [here](https://gitlab.com/gitlab-com/security-tools/report-gsuite-group-members/blob/master/engineering.csv).
1. [ ] New Team Member: Find the recurring Executive Vice President of Engineering's Office Hours event located on the [GitLab Team Meeting Calendar](https://about.gitlab.com/handbook/tools-and-tips/#gitlab-team-meetings-calendar) and copy it over to your personal GitLab calendar.
1. [ ] New team member: Join the ['#eng-week-in-review' channel](https://app.slack.com/client/T02592416/CJWA4E9UG)
1. [ ] New team member: Create an [Iteration Training](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=iteration-training) issue, assign it yourself, and complete the training. 
1. [ ] New team member: Create a [Feature Flag Training](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=feature-flag-training) issue, assign it yourself, and complete the training. 

</details>

##### For Development Department

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Plan to watch the [secure coding training videos](https://about.gitlab.com/handbook/engineering/security/secure-coding-training.html).  Note that they were recorded over two days.  It is suggested you break this up by topic and/or by hour over the next couple weeks.  They cover secure coding practices in general and also cover security risks and mitigations for Ruby on Rails applications.

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: If [baseline entitlements](https://gitlab.com/gitlab-com/team-member-epics/access-requests/tree/master/.gitlab/issue_templates) exist for the new team member's role, create or add to an already created access request using the appropriate template. (This may also cover some other items on this list.)
1. [ ] Manager: Determine if new team member will need access to the [Staging server](https://about.gitlab.com/handbook/engineering/infrastructure/environments/#staging), which is used by engineers to test their changes on a Production-like environment before they land on Production. If so, create or add to an already created [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues) *with the same username the team member has on gitlab.com*.
1. [ ] Manager: Let the new team member know their account on staging.gitlab.com has been created with the same username that they have on gitlab.com.
1. [ ] Manager: Add new team member to respective [`gl-retrospectives`](https://gitlab.com/gl-retrospectives) group
1. [ ] Manager: Add new team member to [team configuration](https://gitlab.com/gitlab-org/async-retrospectives/blob/master/teams.yml) of respective `gl-retrospectives` group
1. [ ] Manager: Add new team member to your [corresponding google group](https://groups.google.com/a/gitlab.com/forum/#!myforums). If the new team member is a manager, give them ownership of their group.
1. [ ] Manager: Provide access to PagerDuty (if applicable).
1. [ ] Manager: (For Distribution) Provide access to the [dev-server](https://gitlab.com/gitlab-com/infrastructure/issues/1592)
1. [ ] Manager: Provide access to [Status Hero](https://statushero.com/integrations/gitlab) for the daily asynchronous standups (if applicable).

</details>
