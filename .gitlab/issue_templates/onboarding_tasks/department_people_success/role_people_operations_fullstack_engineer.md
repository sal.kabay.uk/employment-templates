## People Operations Fullstack Engineer
- [ ] Make yourself familiar with the [People Group Engineering handbook](https://about.gitlab.com/handbook/people-group/engineering/)
- [ ] Read the READMEs for the [People Group Engineering projects](https://gitlab.com/gitlab-com/people-group/peopleops-eng)
