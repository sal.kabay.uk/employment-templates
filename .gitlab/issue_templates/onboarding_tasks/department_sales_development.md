#### Sales Development


<details>
<summary>Manager</summary>

1. [ ] Manager: Create and submit an issue using [this issue template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=leandata_change_sdralignment) to inform Marketing Operations of the territory that the new team member will assume and when lead routing should be started. 
1. [ ] Manager: Send new team member the SDR Participant plan
1. [ ] Manager: Add new team member to the SDR payment calculator sheet under the month of their commencement date with a Quota = 0
1. [ ] Manager: Assign Sales Development Solutions Architect

</details>

<details>
<summary>Sales Development Solutions Architect</summary>

1. [ ] SA: add new hire to Technical Development Course @cs.wang

</details>


<details>
<summary>New Team Member</summary>

1. [ ] New team member: Once you have received your email invite to DataFox, log in and follow [these instructions](https://help.datafox.com/hc/en-us/articles/227081328-User-Setup-Connect-your-DataFox-User-to-your-Salesforce-Account) to link your DataFox account with your Salesforce account. *Note: tool access emails will be sent to you over the course of your first two weeks. 
1. [ ] New SDR team member: Download Google Chrome and set it as your default browser. Google Chrome is the only browser compatible with Outreach, the tool you will be using to daily for this role.
1. [ ] New SDR team member: Please [add your @gitlab.com email address as an alternative](https://www.linkedin.com/help/linkedin/answer/60/adding-or-changing-your-email-address-for-your-linkedin-account?lang=en) on your LinkedIn profile. This will allow you to easily access LinkedIn Sales Navigator once you have received the email invite.
1. [ ] New SDR team member: Connect DiscoverOrg to Salesforce: Once you receive access to DiscoverOrg, log in > click your name in the top right hand corner > click 'Account Settings' > at the top left click 'CRM Settings' > ensure current CRM = Salesforce and Connect to = production instance > click authorize.
1. [ ] New team member: Read through our [SDR Handbook page](/handbook/marketing/revenue-marketing/sdr/). 
1. [ ] New team member: Read through the [GitLab Value Driver Framework](https://docs.google.com/document/d/1GV1WGyJIRuor0jxG-9ABu9ZSIBUFtPq1pqAxV9yJOvQ/edit?usp=sharing)
1. [ ] New SDR team member: Please watch this [recording](https://drive.google.com/file/d/1vsXAxOffFrkb-8CzT8PdANLnCfO6mQfX/view?usp=sharing) running through the [GitLab SDR Foundation Deck](https://docs.google.com/presentation/d/1YRVgVLB3ZESH3ktFmmssrRDIxVvuM2uLTyuZiFfOblo/edit?usp=sharing) 
1. [ ] New SDR team member: [Watch](https://drive.google.com/open?id=1vRgU1o-o4kcOblQCxNi3h6xrN7KQZY1H) John Jeremiah (Product Marketing) deliver the GitLab pitch deck (14 minutes). Review and bookmark the [GitLab pitch deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit?usp=sharing). 
1. [ ] New SDR team member: Please watch this [recording](https://drive.google.com/file/d/1gawbSRknGbPskRB2GfKuL95JbYBlMgR_/view?usp=sharing) running through the [Cloud Native SDR Training Deck](https://docs.google.com/presentation/d/14oKhCP-WHtaYwIgG0AgbNeQAcqUw_Oa8IiysWMQpXPM/edit?usp=sharing)
1. [ ] New team member: Complete your Sales Quickstart Learning Path (see your email for an invitation from Google Classroom). This training was created specifically for our sales team but it is helpful for you to understand our sales motion, the sales workflow and product information. You will run through SDR specific training throughout your onboarding experience with SDR leadership and your team. Please direct SDR specific questions to your manager. If you have questions about SQS, reach out to @tparuchuri.

</details>


<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@tparuchuri): Add new Sales Team Member to Sales Quickstart Learning Path in Google Classroom. New SDR team member will receive an email prompting them to login to Google Classroom to begin working through the Sales Quickstart Learning Path. This Learning Path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.

</details>

