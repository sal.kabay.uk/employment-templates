### Community Advocate

<details>
<summary>New Team Member</summary>

1. [ ] Printfection and Shopify
   1. [ ] New team member: Log into Printfection and Shopify using the credentials saved in the marketing 1password vault, using the merch@gitlab.com email address.
1. [ ] Zendesk
   1. [ ] New team member: Update your [Zendesk account](https://support.zendesk.com/hc/en-us/articles/203690996-Viewing-your-user-profile-in-Zendesk-Support) details
   1. [ ] New team member: Confirm you have set your timezone in your [Zendesk account](https://support.zendesk.com/hc/en-us/articles/203690996-Viewing-your-user-profile-in-Zendesk-Support) profile 
1. [ ] New team member: Create a [Hacker News](https://news.ycombinator.com/news) account if you don't have one already, make sure to specify in your user bio that you're a Community Advocate at GitLab, Hacker News requires that we be transparent about any conflicts of interest.
1. [ ] TweetDeck
   1. [ ] New team member: Open a new [access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request) to request access to Tweetdeck. Remember to include your Twitter handle in the issue description.
   1. [ ] New team member: Enable "Confirmation Step" for all GitLab accounts to prevent accidental tweeting.
1. [ ] Disqus
   1. [ ] New team member: Create a Disqus account, connect it to your `@gitlab.com` Google account and comment your username in the issue.
1. [ ] GitLab Forum
   1. [ ] New team member: Create new account for the [GitLab community forum](https://forum.gitlab.com/) using the sign in with GitLab option and comment your username in the issue.
   1. [ ] New team member: Log into the forum and confirm that you are listed in the [`gitlab-staff`](https://forum.gitlab.com/groups/gitlab-staff) group 
   1. [ ] New team member: Update your forum profile to include at least your picture and your title
1. [ ] New team member: Create an account on [Stack Overflow](http://stackoverflow.com/) and comment your username in the issue.
1. [ ] Reddit:
   1. [ ] New team member: [Create an account on Reddit](https://www.reddit.com/register/?dest=https://www.reddit.com)
   1. [ ] New team member: Add a brief description on your [user profile](https://www.reddit.com/settings/profile) to indicate that you are a Community Advocate at GitLab
   1. [ ] New team member: Comment your username on the issue and tag an /r/gitlab moderator (currently @ecook1) to receive [GitLab employee reddit flair](https://about.gitlab.com/images/handbook/marketing/community-relations/reddit-flair-example.png)
1. [ ] Education, Open Source, Startups program licenses:
   1. [ ] New team member: Open a new [access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request) for dev.gitlab.com to be able to sign into license.gitlab.com and manage Education, OSS and Startups program licenses
   1. [ ] New team member: Open a new [access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request) to customers.gitlab.com to be able check EULA statuses, resend them, set the primary contacts and upgrade groups manually

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: Open a new community advocacy onboarding bootcamp [issue](https://gitlab.com/gitlab-com/marketing/general/issues) using the community advocacy [onboarding checklist](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/onboarding/bootcamp/), and provide the link in a comment in this issue.
1. [ ] 1Password: 
   1. Manager: Open a new [access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request) to add the new team member to the Marketing vault and Marketing group.
1. [ ] Slack:
   1. Manager: Open a new [access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request) to add the new team member to the `@advocates` group
   1. Manager: Open a new [access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request) to add the new team member to the `@community-team` group
1. [ ] Shopify
   1. [ ] Manager: [Add the new member](https://help.shopify.com/manual/your-account/staff-accounts/create-staff-accounts) as a staff user on our Shopify instance
1. [ ] Printfection
   1. [ ] Manager: [Add the new member](https://help.printfection.com/hc/en-us/articles/114094526173-Managing-Users-in-Printfection) as a user on our Printfection account
1. [ ] Zendesk
   1. [ ] Manager: [Add new team member](https://support.zendesk.com/hc/en-us/articles/203661986-Adding-end-users-agents-and-administrators#topic_h43_2k2_yg) as an agent in [GitLab Community ZenDesk](https://gitlab-community.zendesk.com); you may need to [purchase a new license](https://about.gitlab.com/handbook/support/workflows/shared/zendesk/zendesk_admin.html#adding--removing-agents-in-zendesk).
   1. [ ] Manager: Add agent to required [support groups](https://support.zendesk.com/hc/en-us/articles/203661766-About-organizations-and-groups) in [GitLab Community ZenDesk](https://gitlab-community.zendesk.com).
1. [ ] Disqus
   1. [ ] Manager: Give the user the ability to moderate Disqus comments on the blog.
1. [ ] GitLab forum
   1. [ ] Manager: [Grant admin access](https://forum.gitlab.com/admin/users/list/active) to new team member

</details>

