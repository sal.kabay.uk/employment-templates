### For GitLab LTD employees only

<summary>People Experience</summary>

Note: All the below should be done in  one email.
1. [ ] People Experience: Inform Vistra payroll team (1Password -> PeopleOps vault -> Entity & Co-Employer HR Contacts note) of the termination effective date and instruct them to send the individual's P45 to their home address.
1. [ ] People Experience: Notify Vistra HR team that if employee was enrolled into the pension scheme advise payroll of the last day of employment. Ask for confirmation when this is completed.
1. [ ] People Experience: Notify Vistra HR team to remove employee from the Scottish Widows pension scheme (if applicable). Ask for confirmation when this is completed.
1. [ ] People Experience: Notify Vistra HR team to remove employee from the medical insurance effective their termination date. Ask for confirmation when this is completed.
1. [ ] People Experience: Ping Non-US Payroll (@hdevlin @nprecilla) 
