### For CXC Employees and Contractors

<summary>People Experience</summary>

1. [ ] People Experience: For voluntary terms, contact the appropriate CXC representative of the last day of employment (contact details are in People Ops 1Password Vault).
1. [ ] People Experience: Ping Non-US Payroll (@hdevlin @nprecilla) 