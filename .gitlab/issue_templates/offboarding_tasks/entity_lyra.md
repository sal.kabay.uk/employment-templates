### For GitLab BV Lyra only

<summary>People Experience</summary>

1. [ ] People Experience: Inform Lyra HR of the last day of employment.
1. [ ] People Experience: Ping Non-US Payroll (@hdevlin @nprecilla) 
