### For GitLab GmbH employees only

<summary>People Experience</summary>

1. [ ] People Experience: Inform payroll (@hdevlin @nprecilla)  of last day of employment.

