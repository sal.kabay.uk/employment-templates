### FOR PEOPLE OPS ONLY

<summary>People Ops</summary>

1. [ ] People Ops (@brittanyr): Remove team member from BambooHR as an admin.
1. [ ] People Ops (@jkalimon): Remove team member from Cultureamp.

<summary>Manager</summary>

1. [ ] Manager: Remove team member from HR Savvy as an admin.