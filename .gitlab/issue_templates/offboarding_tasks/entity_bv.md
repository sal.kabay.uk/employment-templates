### For GitLab BV employees only

<summary>People Experience</summary>

1. [ ] People Experience: Remove former team member HRSavvy, inform co-employer payrolls.
1. [ ] People Experience: Ping Non-US Payroll (@hdevlin @nprecilla) 
