### FOR SALES ONLY

<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions
      
      * [ ] @Astahn: Update Sale Operations Sponsored Reports/Dashbaords listed on [The Sales Operations Handbook Page](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/#sales-operations-sponsored-dashboards-and-maintenance)
      * [ ] @tav_scott RECORD Snapshot of Salesforce: Export all leads, accounts, contacts, and open opportunities OWNED by the former team member and save as a flat file or googlesheets. Name the file `LASTNAME-FIRSTNAME-OBJECT-YYYY-MM-DD` (Object will be LEADS, ACCOUNTS, CONTACTS, or OPPORTUNITIES).
      * [ ] @tav_scott REASSIGN all leads, accounts, contacts, and open opportunities (do Not Reassign CLOSED WON/LOST OPPS!) based on direction provided by RD/VP within 24 hours of termination. Any named accounts will be transitioned either to RD or designated SAL/AM.
      * [ ] Outreach - @jburton: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
      * [ ] ZoomInfo - @jburton: Deactivate and remove former team member from ZoomInfo
      * [ ]   LeanData - @bethpeterson: Remove from any lead routing rules and round robin groups.
      * [ ]   LinkedIn Sales Navigator - @jburton Disable user, remove from team license.
1. [ ] Rubén or Oswaldo: Remove from admin panel in the [Subscription portal](https://customers.gitlab.com/admin).
1. [ ] Ringlead - Melinda Soares @msoares1: Remove team member from Ringlead if applicable.


<summary>SALs and Named Account owners</summary>

<summary>For Planned Offboarding</summary>

1. [ ] @tav_scott: After notice of termination, SalesOps to work with RD to identify new account and opportunity owner(s). This should happen between termination notice and termination date.
1. [ ] @tav_scott: SalesOps/RD agree to status of Named Accounts. Should these be reassigned to territory owner or another Named Account AE? If former, uncheck Named Accounts; If latter, keep Named Accounts checked.
1. [ ] @bethpeterson: Update Territory Model in LeanData with temporary territory assignments.
1. [ ] @tav_scott: Handbook should be update with temporary territory assignments.
1. [ ] @tav_scott: Once backfilled or transitioned from existing rep, follow that process.

<summary>For Immediate Offboarding</summary>


1. [ ] @tav_scott: Identify name, term dates, and territories. This comes from PeopleOps.
1. [ ] @tav_scott: SalesOps to work with RD to identify new account and opportunity owner(s). This should happen within 24 hours of termination.
1. [ ] @tav_scott: SalesOps/RD agree to status of Named Accounts. Should these be reassigned to territory owner or another Named Account AE? If former, uncheck Named Accounts; if latter, keep Named Accounts checked.
1. [ ] @tav_scott: Update SDRs if applicable.
1. [ ] @bethpeterson: Update Territory Model in LeanData with temporary territory assignments.
1. [ ] @tav_scott: Handbook should be update with temporary territory assignments.
1. [ ] @tav_scott: Once backfilled or transitioned from existing rep, follow that process.
